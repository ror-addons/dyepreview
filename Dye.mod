<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="Dye Preview" version="1.3.2" date="08/06/2012" >
        <Author name="Wothor; Aborea" email="" />
        <Description text="Dye preview type / yak: toggle button added to characterframe" />
        <VersionSettings gameVersion="1.4.6" windowsVersion="1.0" savedVariablesVersion="1.0" />
        <Dependencies>
			<Dependency name="EA_CharacterWindow" optional="false" forceEnable="true" />
			<Dependency name="LibSlash" optional="true" forceEnable="true" />
		</Dependencies>
        <Files>
			<File name="Dye.xml" />
            <File name="Dye.lua" />
        </Files>
        <OnInitialize>
            <CallFunction name="Dye.Initialize" />
        </OnInitialize>
        <OnUpdate />
        <OnShutdown />
		<WARInfo>
			<Categories>
				<Category name="ITEMS_INVENTORY" />
				<Category name="OTHER" />
			</Categories>
			<Careers>
				<Career name="BLACKGUARD" />
				<Career name="WITCH_ELF" />
				<Career name="DISCIPLE" />
				<Career name="SORCERER" />
				<Career name="IRON_BREAKER" />
				<Career name="SLAYER" />
				<Career name="RUNE_PRIEST" />
				<Career name="ENGINEER" />
				<Career name="BLACK_ORC" />
				<Career name="CHOPPA" />
				<Career name="SHAMAN" />
				<Career name="SQUIG_HERDER" />
				<Career name="WITCH_HUNTER" />
				<Career name="KNIGHT" />
				<Career name="BRIGHT_WIZARD" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="CHOSEN" />
				<Career name="MARAUDER" />
				<Career name="ZEALOT" />
				<Career name="MAGUS" />
				<Career name="SWORDMASTER" />
				<Career name="SHADOW_WARRIOR" />
				<Career name="WHITE_LION" />
				<Career name="ARCHMAGE" />
			</Careers>
		</WARInfo>
    </UiMod>
</ModuleFile>
