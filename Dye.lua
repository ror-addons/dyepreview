Dye = {}

local items ={L"All", L"Body", L"Gloves", L"Boots", L"Helm", L"Shoulders", L"Belt"}
local dyes = {
	L"Azyr Blue", L"Bestial Brown", L"Blazing Orange", L"Blood Red", L"Bone Brown", L"Brazen Brass",
	L"Chainmail Grey", L"Chaos Black", L"Dark Ivory Dye", L"Dark Navy Blue", L"Dark Peach Dye", 
	L"Dark Purple", L"Dark Red", L"Dark Sage", L"Dark Sea Blue", L"Dark Seafoam Dye", L"Dark Warm Sage",
	L"Enchanted Blue", L"Fortress Grey", L"Goblin Green", L"Golden Yellow",	L"Graveyard Earth", 
	L"Liche Purple", L"Light Grey Dye", L"Light Ivory Dye", L"Light Sage Dye", 
	L"Medium Olive Dye", L"Medium Red Dye", L"Medium Sky Blue Dye", L"Medium Yellow Dye", L"Midnight Blue", 
	L"Orc Green", L"Red Gore", L"Regal Blue", L"Scab Red", L"Scaly Green", L"Scorched Brown", L"Seaguard Blue", 
	L"Shadow Grey",L"Shyish Purple", L"Skull White", L"Snot Green", L"Tanned Flesh", L"Ulgu Grey", 
	L"Vermin Brown", L"Warlock Purple"
}
	
local dyenr = {
	213, 193, 198, 187, 192, 197, 179, 181, 40, 4, 25, 37, 31, 13, 7, 10, 61, 214, 183, 205, 196, 190,
	216, 45, 42, 15, 17, 32, 2, 20, 212, 203, 188, 211, 219, 207, 185, 209, 208, 217, 180, 204, 194,
	182, 195, 215
}
			
function Dye.Initialize()
	if LibSlash then LibSlash.RegisterSlashCmd("dye", function() WindowSetShowing("Dye", true) end ) end

	CreateWindow("Dye",false)
	CreateWindow("DyeToogleButton",true)
	WindowSetParent("DyeToogleButton", "CharacterWindow")
	
	-- Write text in the label
	LabelSetText("DyePrimaryColor", L"Primary color")
	LabelSetText("DyeSecondaryColor", L"Secondary color")
	LabelSetText("DyeItems", L"Item to dye")
	ButtonSetText("DyeBtn1", L"Default")
	
	--add items to dropdown boxes
	for _, v in ipairs (dyes) do
		ComboBoxAddMenuItem ("DyeCombo", v)
	end
	for _, v in ipairs (dyes) do
		ComboBoxAddMenuItem ("DyeCombo2", v)
	end
	for _, v in ipairs (items) do
		ComboBoxAddMenuItem ("DyeCombo3", v)
	end
	ComboBoxSetSelectedMenuItem ("DyeCombo3", 1)
	ComboBoxSetSelectedMenuItem ("DyeCombo2", 1)
	ComboBoxSetSelectedMenuItem ("DyeCombo", 1)
end

function Dye.OnSelChanged()
	local sel = ComboBoxGetSelectedMenuItem("DyeCombo3")
	local primary = dyenr[ComboBoxGetSelectedMenuItem("DyeCombo")]
	local secondary = dyenr[ComboBoxGetSelectedMenuItem("DyeCombo2")]
	
	if sel == 1 then
		DyeMerchantPreviewAll(primary, secondary)
	elseif sel == 2 then
		DyeMerchantPreview( GameData.ItemLocs.EQUIPPED, 6, primary, secondary)
	elseif sel == 3 then
		DyeMerchantPreview( GameData.ItemLocs.EQUIPPED, 7, primary, secondary)
	elseif sel == 4 then
		DyeMerchantPreview( GameData.ItemLocs.EQUIPPED, 8, primary, secondary)
	elseif sel == 5 then
		DyeMerchantPreview( GameData.ItemLocs.EQUIPPED, 9, primary, secondary)
	elseif sel == 6 then
		DyeMerchantPreview( GameData.ItemLocs.EQUIPPED, 10, primary, secondary)
	elseif sel == 7 then
		DyeMerchantPreview( GameData.ItemLocs.EQUIPPED, 14, primary, secondary)
	end
end

function Dye.Reset() RevertAllDyePreview() end
function Dye.Hide() WindowSetShowing("Dye", false) end
function Dye.Toogle() WindowSetShowing("Dye", not WindowGetShowing("Dye")) end